.data
str1: .asciiz "Enter an integer:"
str2: .asciiz "\nEnter another integer:"
str3: .asciiz "\nThe greatest common divisor is:"
.text

main:

li $v0,4			
la $a0, str1
syscall

li $v0,5
syscall
add $a0,$zero,$v0

li $v0,4			
la $a0, str2
syscall

li $v0,5
syscall			
add $t1,$zero,$v0
li $v0,1
syscall

li $v0,10
syscall

GCDLoop:

div $t0,$t1
mfhi $t2
add $t0,$zero,$t1
add $t1,$zero,$t2
bne $t1,$zero,GCDLoop

finish:
li $v0,4			
la $a0, str3
syscall
add $t0,$zero,$t0


