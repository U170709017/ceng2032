.data
str1: .asciiz "Enter an integer:"
str2: .asciiz "\nEnter another integer:"
str3: .asciiz "\nThe greatest common divisor is:"

.text
.globl main
main:
li $v0,4			
la $a0, str1
syscall

li $v0,5
syscall
addi $a0,$v0,0   

li $v0,4			
la $a0, str2
syscall

li $v0,5
syscall			
addi $t1,$v0,0 



GCDRec:
       beq $a0,$a1,finish
       addi $sp,$sp,-4
       sw $ra,0($sp)
       slt $t0,$a0,$a1
       beq $t0,$s0,L
       sub $a1,$a1,$a0
       jal GCDRec
jr $ra

L:
sub $a0,$a0,$a1
jal GCDRec
jr $ra

lw $ra,0($sp)
addi $sp,$sp,4
finish:
li $v0,4			
la $a0, str3
syscall
add $t0,$zero,$t0

li $v0,1
syscall

li $v0,10
syscall






















